package com.aoptest.aoptest.utils;

import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;

public class CustomInterceptor implements MethodInterceptor {
    @Override
    public Object invoke(MethodInvocation methodInvocation) throws Throwable {
        AopInterceptorAnnotation annotation = methodInvocation.getMethod().getAnnotation(AopInterceptorAnnotation.class);
        if(annotation != null){
            System.out.println(annotation.value() + " success!");
        }
        Object proceed = methodInvocation.proceed();

        System.out.println("Will be return: " + proceed);

        return proceed;
    }
}
