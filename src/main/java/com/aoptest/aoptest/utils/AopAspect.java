package com.aoptest.aoptest.utils;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

import java.util.Arrays;

@Component
@Aspect
public class AopAspect {
    @Pointcut("execution(* com.aoptest.aoptest..*.*(..))")
    public void webServiceMethod() {
    }

    @Pointcut("@annotation(com.aoptest.aoptest.utils.AopAnnotation)")
    public void loggableMethod() {
    }

    @Around("webServiceMethod() && loggableMethod()")
    public Object logWebServiceCall(ProceedingJoinPoint pjp) throws Throwable {
        String methodName = pjp.getSignature().getName();
        Object[] methodArgs = pjp.getArgs();

        System.out.println("Call method " + methodName + " with args " + Arrays.asList(methodArgs));

        Object result = pjp.proceed();

        System.out.println("Method " + methodName + " returns " + result);
        return result;
    }

}
