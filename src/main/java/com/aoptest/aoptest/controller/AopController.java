package com.aoptest.aoptest.controller;

import com.aoptest.aoptest.service.ServiceInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class AopController {

    @Autowired
    private ServiceInterface serviceInterface;

    public String testAopExecution(){
        System.out.println(serviceInterface.saySomethingAround());
        System.out.println(serviceInterface.saySomethingInterception());
        return "";
    }

}
