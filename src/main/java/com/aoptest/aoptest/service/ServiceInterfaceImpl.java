package com.aoptest.aoptest.service;

import com.aoptest.aoptest.utils.AopAnnotation;
import com.aoptest.aoptest.utils.AopInterceptorAnnotation;
import org.springframework.stereotype.Service;

@Service
public class ServiceInterfaceImpl implements ServiceInterface {

    @Override
    @AopAnnotation
    public String saySomethingAround() {
        return "Something Around";
    }

    @Override
    @AopInterceptorAnnotation(value = "test")
    public String saySomethingInterception() {
        return "Something Interception";
    }
}
