package com.aoptest.aoptest.service;

public interface ServiceInterface {

    String saySomethingAround();

    String saySomethingInterception();

}
