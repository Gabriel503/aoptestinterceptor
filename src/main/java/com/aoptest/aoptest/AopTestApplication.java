package com.aoptest.aoptest;

import com.aoptest.aoptest.controller.AopController;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class AopTestApplication {

	public static void main(String[] args) {
		ConfigurableApplicationContext context = SpringApplication.run(AopTestApplication.class, args);
		AopController bean = context.getBean(AopController.class);
		System.out.println(bean.testAopExecution());
	}
}
