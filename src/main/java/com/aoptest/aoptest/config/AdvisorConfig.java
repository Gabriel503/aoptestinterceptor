package com.aoptest.aoptest.config;

import com.aoptest.aoptest.utils.CustomInterceptor;
import org.springframework.aop.Advisor;
import org.springframework.aop.aspectj.AspectJExpressionPointcut;
import org.springframework.aop.support.DefaultPointcutAdvisor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AdvisorConfig {

    @Bean
    public Advisor advisor() {
        AspectJExpressionPointcut pointcut = new AspectJExpressionPointcut();
        pointcut.setExpression("@annotation(com.aoptest.aoptest.utils.AopInterceptorAnnotation)");//AopInterceptorAnnotation
        return new DefaultPointcutAdvisor(pointcut, new CustomInterceptor());
    }

}
